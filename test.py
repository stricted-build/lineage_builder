import contextlib
import datetime
import os
import sys
import unittest

import flask_migrate
import flask_testing

from ui import config
from ui.app import app
from ui.models import Build, Runner, db

class UsesApp(flask_testing.TestCase):
    def create_app(self):
        return app

class UsesModels(UsesApp):
    def setUp(self):
        with open(os.devnull, "w") as f, contextlib.redirect_stderr(f):
            flask_migrate.upgrade(revision="head")

    def tearDown(self):
        with open(os.devnull, "w") as f, contextlib.redirect_stderr(f):
            flask_migrate.downgrade(revision="base")

class TestModels(UsesModels):

    def test_build(self):
        build = Build(build_id=1)
        db.session.add(build)
        db.session.commit()

        assert build in db.session

    def test_get_or_create_build_by_id(self):
        build = Build(build_id=1)
        db.session.add(build)
        db.session.commit()

        assert Build.get_or_create_by_id(1).build_id == 1
        assert Build.get_or_create_by_id(2).build_id == 2

    def test_runner(self):
        runner = Runner(runner_id="foobar")
        db.session.add(runner)
        db.session.commit()

        assert runner in db.session

    def test_get_or_create_runner_by_id(self):
        runner = Runner(runner_id="foobar", runner_name="foobar")
        db.session.add(runner)
        db.session.commit()

        assert Runner.get_or_create_by_id('foobar').runner_name == "foobar"
        assert Runner.get_or_create_by_id("foobaz").runner_name == None

    def test_runner_build_rel(self):
        runner = Runner(runner_id="foobar", runner_name="foobar")
        build = Build(build_id=1, build_runner=runner)
        db.session.add(runner)
        db.session.add(build)
        db.session.commit()

        assert build.build_runner == runner
        assert build in runner.builds

class TestWebhooks(UsesModels):

    def test_invalid_token(self):
        with open("tests/pipeline_webhook.json", "r") as f:
            response = self.client.post("/webhook", headers={'X-Gitlab-Event': 'Pipeline Hook', 'X-Gitlab-Token': 'invalidsecret'}, data=f.read(), content_type='application/json')
        assert response.status_code == 403

    def test_pipeline_webhook(self):
        with open("tests/pipeline_webhook.json", "r") as f:
            response = self.client.post("/webhook", headers={'X-Gitlab-Event': 'Pipeline Hook', 'X-Gitlab-Token': 'secret'}, data=f.read(), content_type='application/json')
        assert response.status_code == 200
        build = Build.query.filter_by(build_id=1).first()
        assert build.build_id == 1
        assert build.build_runner_id == None

    def test_success(self):
        with open("tests/pipeline_success.json") as f:
            response = self.client.post("/webhook", headers={'X-Gitlab-Event': 'Pipeline Hook', 'X-Gitlab-Token': 'secret'}, data=f.read(), content_type='application/json')
        assert response.status_code == 200
        runner = Runner.query.filter_by(runner_id="530365").first()
        build = Build.query.filter_by(build_id=34618162).first()
        assert build.build_runner.runner_name == "phenom-test"
        assert build.build_version == "lineage-15.1"
        assert build.build_device == "FAKE_TEST"
        assert build.build_status == "success"
        assert build.build_duration == 81


    def test_start(self):
        with open("tests/pipeline_start.json") as f:
            response = self.client.post("/webhook", headers={'X-Gitlab-Event': 'Pipeline Hook', 'X-Gitlab-Token': 'secret'}, data=f.read(), content_type='application/json')
        assert response.status_code == 200


class TestWeb(UsesModels):

    def test_get(self):
        build1 = Build(build_id=1, build_date=datetime.datetime.strptime("2018-01-01", "%Y-%m-%d"), build_status="pending", build_version="cm-14.1")
        runner = Runner(runner_name="foobar", runner_id="foobar", runner_sponsor="Me", runner_sponsor_url="You")
        build2 = Build(build_id=2, build_status="success", build_device="mako", build_version="cm-14.1", build_type="userdebug", build_date=datetime.datetime.strptime("2018-01-01", "%Y-%m-%d"), build_runner=runner, build_duration=2)
        db.session.add(build1)
        db.session.add(runner)
        db.session.add(build2)
        db.session.commit()
        response = self.client.get("/")
        assert response.status_code == 200

        response = self.client.get("/runners/")
        assert response.status_code == 200

        response = self.client.get("/runners/foobar")
        assert response.status_code == 200

        response = self.client.get("/?status=success&device=mako")
        assert response.status_code == 200

        response = self.client.get("/api/v1/runners")
        assert response.status_code == 200

        response = self.client.get("/api/v1/builds")
        assert response.status_code == 200

        response = self.client.get("/api/v1/stats")
        assert response.status_code == 200

        response = self.client.get("/stats")
        assert response.status_code == 200

if __name__ == "__main__":
    unittest.main()
