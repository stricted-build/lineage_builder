"""add duration

Revision ID: 448614888e24
Revises: c3a5cacf3bff
Create Date: 2018-10-28 17:34:59.818607

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '448614888e24'
down_revision = 'c3a5cacf3bff'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('build', sa.Column('build_duration', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('build') as batch_op:
        batch_op.drop_column('build_duration')
#    op.drop_column('build', 'build_duration')
    # ### end Alembic commands ###
