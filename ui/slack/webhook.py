import requests

from ui import config


def post_build(status, device, version, btype, id_):
    if not config.SLACK_WEBHOOK_URL:
        return
    if status == "failed":
        state = "has failed for"
    elif status == "canceled":
        state = "was canceled on"
    else:
        return
    version = version.split("-")[1]
    text = "LineageOS {} {} {} ({})".format(version, state, device, btype)
    data = {
        "channel": "#releases",
        "attachments": [
            {
                "fallback": text,
                "author_name": "buildbot",
                "title": text,
                "text": "https://gitlab.com/lineageos/builder/android/pipelines/{}".format(id_),
                "color": "danger" if status == "failed" else "#020202"
            }
        ]
    }
    requests.post(config.SLACK_WEBHOOK_URL, json=data)

if __name__ == "__main__":
    post_build('failed', 'mako', 'lineage-40.1', 'userdebug', 4)
    post_build('canceled', 'mako', 'lineage-40.1', 'stable', 5)
