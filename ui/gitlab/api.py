from datetime import datetime
import requests
import os

GITLAB_BASE = "https://gitlab.com/api/v4/"

def create_pipeline(project, trigger, data):
    resp = requests.post("{}projects/{}/trigger/pipeline?token={}".format(GITLAB_BASE, project, token), data=data)
    if resp.status_code != 201:
        raise Exception("ERROR", req.status_code, req.json())
    return resp.json()
