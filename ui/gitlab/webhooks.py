import datetime

from flask import abort

from ui import config
from ui.gitlab import api
from ui.models import Build, Runner, db
from ui.slack.webhook import post_build

def process(request):
    if request.headers.get('X-Gitlab-Token', None) != config.GITLAB_WEBHOOK_TOKEN:
        abort(403)
    webhook_type = request.headers.get('X-Gitlab-Event')
    data = request.get_json()
    if webhook_type == 'Pipeline Hook':
        pipeline = data.get('object_attributes')
        stages = data.get('builds')
        build = Build.get_or_create_by_id(pipeline.get('id'))
        build.build_status = pipeline.get('status')
        if build.build_status == "success":
            build.build_duration = pipeline.get('duration')
        for variable in pipeline.get("variables"):
            if variable.get('key') == "VERSION":
                build.build_version = variable.get("value")
            elif variable.get("key") == "DEVICE":
                build.build_device = variable.get("value")
            elif variable.get("key") == "TYPE":
                build.build_type = variable.get("value")
        build_stage = {}
        for stage in stages:
            if stage.get('name') == 'build':
                build_stage = stage
        runner = build_stage.get("runner")
        if runner:
            build.build_runner = Runner.get_or_create_by_id(str(runner.get('id')))
            if not build.build_runner.runner_name:
                build.build_runner.runner_name = runner.get("description")
        date = datetime.datetime.strptime(build_stage.get("created_at"), "%Y-%m-%d %H:%M:%S UTC")
        build.build_date = date
        db.session.add(build)
        db.session.commit()
        if build.build_status in ['failed', 'canceled']:
            post_build(build.build_status, build.build_device, build.build_version, build.build_type, build.build_id)
