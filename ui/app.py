import datetime
import os

import requests
from flask import Flask, render_template, request, abort, jsonify
from flask_bootstrap import Bootstrap
from flask_caching import Cache
from flask_migrate import Migrate
from flask_nav import Nav
from flask_nav.elements import Navbar, Text, View
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import orm, func

from ui import gitlab, config, models

app = Flask(__name__)
Bootstrap(app)
app.config.from_object(config)
cache = Cache(app)
models.db.init_app(app)
migrate = Migrate(app, models.db)
nav = Nav(app)

nav.register_element('top', Navbar(
    "LineageOS Builds",
    View('Builds', '.web_index'),
    View('Runners', '.web_runners'),
    View('Devices', '.web_devices'),
    View('Stats', '.web_stats'),
))

headers = {'Private-Token': os.environ.get('GITLAB_TOKEN', '')}

def version():
    return os.environ.get("VERSION", "dev")[:6]

app.jinja_env.globals.update(version=version)

def parse_args():
    args = {}
    if request.args:
        if 'status' in request.args:
            args['build_status'] = request.args.get('status')
        if 'device' in request.args:
            args['build_device'] = request.args.get('device')
        if 'version' in request.args:
            args['build_version'] = request.args.get('version')
        if 'type' in request.args:
            args['build_type'] = request.args.get('type')
        if 'date' in request.args:
            date = datetime.datetime.strptime(request.args.get('date'), '%Y-%m-%d').date()
            args['build_date'] = datetime.datetime.strptime(request.args.get('date'), '%Y-%m-%d').date()
        if 'id' in request.args:
            args['build_id'] = request.args.get('id')
    return args

@cache.memoize()
def stats():

    runner_build_times = models.Build.query.join(models.Build.build_runner).with_entities(
        models.Runner.runner_name,
        models.Build.build_version,
        func.avg(models.Build.build_duration),
        func.max(models.Build.build_duration),
        func.min(models.Build.build_duration),
        func.sum(models.Build.build_duration)
    ).group_by(models.Build.build_version, models.Runner.runner_name).all()

    all_build_times = models.Build.query.with_entities(
        models.Build.build_version,
        func.avg(models.Build.build_duration),
        func.max(models.Build.build_duration),
        func.min(models.Build.build_duration),
        func.sum(models.Build.build_duration)
    ).group_by(models.Build.build_version).all()

    runner_build_status = models.Build.query.join(models.Build.build_runner).with_entities(
        models.Runner.runner_name,
        models.Build.build_status,
        func.count(models.Build.build_status)
    ).group_by(models.Runner.runner_name, models.Build.build_status).all()


    stats = {
        'builds': {
            'all': {}
        },
        'times': {
            'all': {}
        }
    }

    for build_time in all_build_times:
        stats['times']['all'][build_time[0]] = {
            'avg': build_time[1] if build_time[1] else 0,
            'max': build_time[2] if build_time[2] else 0,
            'min': build_time[3] if build_time[3] else 0,
            'sum': build_time[4] if build_time[4] else 0,
        }

    for build_time in runner_build_times:
        stats['times'].setdefault(build_time[0], {})[build_time[1]] = {
            'avg': build_time[2] if build_time[2] else 0,
            'max': build_time[3] if build_time[3] else 0,
            'min': build_time[4] if build_time[4] else 0,
            'sum': build_time[5] if build_time[5] else 0,
        }

    for build_status in runner_build_status:
        stats['builds']['all'].setdefault(build_status[1], 0)
        stats['builds']['all'][build_status[1]] += build_status[2]

        stats['builds'].setdefault(build_status[0], {})[build_status[1]] = build_status[2]
    return stats

@app.route('/')
def web_index():
    try:
        args = parse_args()
    except ValueError:
        return "Invalid Date", 400
    builds = models.Build.paginate(args)
    return render_template('builds.html', builds=builds)

@app.route('/runners/<string:runner>')
def web_runner(runner):
    try:
        args = parse_args()
    except ValueError:
        return "Invalid Date", 400
    runner = models.Runner.get({'runner_name': runner}).first()
    args['build_runner'] = runner
    builds = models.Build.paginate(args)
    return render_template('runner.html', runner=runner, builds=builds)

@app.route('/stats')
def web_stats():
    stats_ = stats()
    runners = ['all'] + [x for x in sorted(stats_['builds'].keys()) if x != 'all']
    return render_template('stats.html', stats=stats_, runners=runners)

@app.route("/runners/")
def web_runners():
    #select * from runner join (select * from build where build_status = "success" order by build_date) as builds on builds.build_runner_id = runner.runner_id group by build_runner_id;
    subquery = models.Build.query.filter(models.Build.build_status == "success").order_by(models.Build.build_date).subquery()
    runners = models.Runner.query.outerjoin(
        subquery, subquery.c.build_runner_id == models.Runner.runner_id
    ).with_entities(
        models.Runner.runner_id,
        models.Runner.runner_name,
        models.Runner.runner_sponsor,
        models.Runner.runner_sponsor_url,
        subquery.c.build_date
    ).group_by(models.Runner.runner_id).order_by(subquery.c.build_date.desc(), models.Runner.runner_name).all()
    return render_template('runners.html', runners=runners)

@app.route("/devices/")
def web_devices():
    builds = models.Build.query.filter(models.Build.build_date > datetime.date.today() - datetime.timedelta(90)).group_by(models.Build.build_device).having(func.max(models.Build.build_date)).order_by(func.lower(models.Build.build_device)).all()
    return render_template("devices.html", builds=builds)

@app.route('/api/v1/builds')
def api_builds():
    try:
        args = parse_args()
    except ValueError:
        return jsonify({'error': 'Invalid Date'}), 400
    builds = models.Build.paginate(args).items
    if not builds:
        abort(404)
    return jsonify([x.as_dict() for x in builds])

@app.route('/api/v1/runners')
def api_runners():
    runners = models.Runner.get().all()
    if not runners:
        abort(404)
    return jsonify([x.as_dict() for x in runners])

@app.route('/api/v1/runners/<string:runner>')
def api_runner(runner):
    try:
        args = parse_args()
    except ValueError:
        return jsonify({"Invalid Date"}), 400
    runner = models.Runner.get({"runner_name": runner}).first()
    if not runner:
        abort(404)
    return jsonify(runner.as_dict())

@app.route('/api/v1/stats')
def api_stats():
    return jsonify(stats())

@app.route('/stats')
@app.route("/webhook", methods=('POST',))
def process_webhook():
    gitlab.webhooks.process(request)
    return "OK", 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
